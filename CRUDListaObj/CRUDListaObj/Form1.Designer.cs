﻿namespace CRUDListaObj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBoxNomes = new System.Windows.Forms.ComboBox();
            this.ComboBoxListaAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.TextBoxApelidoAluno = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxNomeAluno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBoxNomes);
            this.groupBox1.Controls.Add(this.ComboBoxListaAlunos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(409, 116);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagens";
            // 
            // ComboBoxNomes
            // 
            this.ComboBoxNomes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxNomes.FormattingEnabled = true;
            this.ComboBoxNomes.Location = new System.Drawing.Point(17, 80);
            this.ComboBoxNomes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBoxNomes.Name = "ComboBoxNomes";
            this.ComboBoxNomes.Size = new System.Drawing.Size(261, 25);
            this.ComboBoxNomes.TabIndex = 1;
            // 
            // ComboBoxListaAlunos
            // 
            this.ComboBoxListaAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaAlunos.FormattingEnabled = true;
            this.ComboBoxListaAlunos.Location = new System.Drawing.Point(17, 37);
            this.ComboBoxListaAlunos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBoxListaAlunos.Name = "ComboBoxListaAlunos";
            this.ComboBoxListaAlunos.Size = new System.Drawing.Size(372, 25);
            this.ComboBoxListaAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonProcurar);
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(430, 56);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(234, 103);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.Location = new System.Drawing.Point(51, 62);
            this.ButtonProcurar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(134, 32);
            this.ButtonProcurar.TabIndex = 2;
            this.ButtonProcurar.Text = "Procurar";
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            this.ButtonProcurar.Click += new System.EventHandler(this.ButtonProcurar_Click);
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(122, 24);
            this.TextBoxProcurar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(109, 23);
            this.TextBoxProcurar.TabIndex = 1;
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "Id Aluno",
            "Nome"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(11, 24);
            this.ComboBoxProcurar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(92, 25);
            this.ComboBoxProcurar.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ButtonCancelar);
            this.groupBox3.Controls.Add(this.ButtonGravar);
            this.groupBox3.Controls.Add(this.ButtonEditar);
            this.groupBox3.Controls.Add(this.TextBoxApelidoAluno);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.textBoxPrimeiroNome);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.TextBoxNomeAluno);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.TextBoxIdAluno);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(9, 177);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(655, 103);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Editar";
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.Location = new System.Drawing.Point(520, 24);
            this.ButtonEditar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(122, 33);
            this.ButtonEditar.TabIndex = 8;
            this.ButtonEditar.Text = "Editar";
            this.ButtonEditar.UseVisualStyleBackColor = true;
            this.ButtonEditar.Click += new System.EventHandler(this.ButtonEditar_Click);
            // 
            // TextBoxApelidoAluno
            // 
            this.TextBoxApelidoAluno.Enabled = false;
            this.TextBoxApelidoAluno.Location = new System.Drawing.Point(354, 67);
            this.TextBoxApelidoAluno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBoxApelidoAluno.Name = "TextBoxApelidoAluno";
            this.TextBoxApelidoAluno.Size = new System.Drawing.Size(124, 23);
            this.TextBoxApelidoAluno.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(282, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Apelido : ";
            // 
            // textBoxPrimeiroNome
            // 
            this.textBoxPrimeiroNome.Enabled = false;
            this.textBoxPrimeiroNome.Location = new System.Drawing.Point(87, 70);
            this.textBoxPrimeiroNome.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPrimeiroNome.Name = "textBoxPrimeiroNome";
            this.textBoxPrimeiroNome.Size = new System.Drawing.Size(124, 23);
            this.textBoxPrimeiroNome.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nome  : ";
            // 
            // TextBoxNomeAluno
            // 
            this.TextBoxNomeAluno.Location = new System.Drawing.Point(353, 29);
            this.TextBoxNomeAluno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBoxNomeAluno.Name = "TextBoxNomeAluno";
            this.TextBoxNomeAluno.ReadOnly = true;
            this.TextBoxNomeAluno.Size = new System.Drawing.Size(125, 23);
            this.TextBoxNomeAluno.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome Aluno : ";
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(100, 29);
            this.TextBoxIdAluno.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(110, 23);
            this.TextBoxIdAluno.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Aluno : ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.actualizarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(692, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.apagarToolStripMenuItem});
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.alunoToolStripMenuItem.Text = "Aluno";
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
            // 
            // apagarToolStripMenuItem
            // 
            this.apagarToolStripMenuItem.Name = "apagarToolStripMenuItem";
            this.apagarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.apagarToolStripMenuItem.Text = "Apagar";
            this.apagarToolStripMenuItem.Click += new System.EventHandler(this.apagarToolStripMenuItem_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.actualizarToolStripMenuItem.Text = "Actualizar";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Enabled = false;
            this.ButtonCancelar.Image = global::CRUDListaObj.Properties.Resources.ic_cancel;
            this.ButtonCancelar.Location = new System.Drawing.Point(586, 62);
            this.ButtonCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(56, 37);
            this.ButtonCancelar.TabIndex = 10;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.Enabled = false;
            this.ButtonGravar.Image = global::CRUDListaObj.Properties.Resources.ic_ok;
            this.ButtonGravar.Location = new System.Drawing.Point(520, 62);
            this.ButtonGravar.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(56, 37);
            this.ButtonGravar.TabIndex = 9;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 290);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form Principal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxListaAlunos;
        private System.Windows.Forms.ComboBox ComboBoxNomes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.TextBox TextBoxApelidoAluno;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPrimeiroNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxNomeAluno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
    }
}

