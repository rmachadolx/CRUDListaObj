﻿namespace CRUDListaObj
{
    using Modelos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarAluno : Form
    {
        private List<Aluno> ListaDeAlunos;
        private Form1 formPrincipal;

        private Aluno alunoAApagar;

        public FormApagarAluno(List<Aluno>listaAlunos, Aluno aluno)
        {
            InitializeComponent();

            this.formPrincipal = formPrincipal;

            this.ListaDeAlunos = listaAlunos;
            alunoAApagar = aluno;

            DataGridViewAlunoApagar.Rows.Add(
                alunoAApagar.IdAluno, 
                alunoAApagar.PrimeiroNome, 
                alunoAApagar.Apelido);

            DataGridViewAlunoApagar.AllowUserToAddRows = false;

        }

        private void ButtonApagar_Click(object sender, System.EventArgs e)
        {

            int index = 0;

            foreach(var aluno in ListaDeAlunos)
            {
                if(alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index);
                    break;
                }

                index++;
            }

            MessageBox.Show("Aluno apagado com sucesso.");

            Close();
        }

        private void ButtonCancelar_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("O aluno não foi apagado.");
            Close();
        }
    }
}