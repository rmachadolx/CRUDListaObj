﻿namespace CRUDListaObj
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormNovoAluno : Form
    {
        private List<Aluno> listaDeAlunos = new List<Aluno>();

        public FormNovoAluno(List<Aluno>listaDeAlunos)
        {
            InitializeComponent();
            this.listaDeAlunos = listaDeAlunos;
        }

        private void ButtonNovoAluno_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Tem que inserir o primeiro nome do aluno.");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxApelido.Text))
            {
                MessageBox.Show("Tem que inserir o apelido do aluno.");
                return;
            }

            var aluno = new Aluno
            {
                IdAluno = GeraIdAluno(),
                PrimeiroNome = TextBoxPrimeiroNome.Text,
                Apelido = TextBoxApelido.Text
            };

            listaDeAlunos.Add(aluno);
            MessageBox.Show("Novo aluno inserido com sucesso");
            Close();
        }

        private int GeraIdAluno()
        {
            return listaDeAlunos[listaDeAlunos.Count - 1].IdAluno + 10;
        }
    }
}
