﻿using System.Runtime.InteropServices.WindowsRuntime;

namespace CRUDListaObj.Modelos
{
    public class Aluno
    {
        public int IdAluno { get; set; }

        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public string NomeCompleto
        {
            get { return string.Format("{0} {1}", PrimeiroNome, Apelido); }
        }

        #region Métodos Genéricos

        public override string ToString()
        {
            return string.Format("Id Aluno: {0}  Nome: {1}", IdAluno, NomeCompleto);
        }

        #endregion

    }
}
